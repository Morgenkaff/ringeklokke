#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// SSID og password til wifi netværk
const char* ssid = "esp-net";
const char* password = "esp-kode";

// Variabler brugt af logik der tjekker for wifi forbindelse (i slutning af koden)
unsigned long previousMillis = 0;
unsigned long interval = 30000;

// Funktion der starter wifi og forbinder
void initWiFi() {
  // Sæt mode til "station". Station = client, i modsætning til access point.
  WiFi.mode(WIFI_STA);
  // Begin = "connect to", samt credentials fra foroven
  WiFi.begin(ssid, password);
  // Giv feedback til serial monitor - hjælper på utålmodighed
  Serial.print("Connecting to WiFi ..");
  // Så længe wifi ikke er forbundet: blink langsomt
  while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      digitalWrite(D0, HIGH); 
      delay(500);
      Serial.print(".");
      digitalWrite(D0, LOW); 
  }
  // Når wifi er forbundet: blink hurtigt (funktion)
  connection_blink(4);
  // Print enheds IP til serial monitor - til debug
  Serial.println(WiFi.localIP());
  // Næste 2 linjer sættes til at prøve at forbinde igen hvis wifi ryger
  // (der er ekstra fail safe nederst i koden også)
  WiFi.setAutoReconnect(true);
  WiFi.persistent(true);
}

// Variabler der bruges til MQTT:
// IP til MQTT serveren (Raspberry Pi)
const char *MQTT_HOST = "192.168.4.1";
// Port der bruges af MQTT serveren
const int MQTT_PORT = 1883;
// Denne enheds ID/navn (skal være unikt)
const char *MQTT_CLIENT_ID = "ESP8266_1";
// Der skal være en bruger ifm MQTT. Det bruges ikke rigtig i det her setup,
// da det ikke er nødvændigt at holde data skjult imellem enhederne.
// Men brugeren skal være der
const char *MQTT_USER = "esp";
// Og den bruger skal have en kode
const char *MQTT_PASSWORD = "esp-kode";
// Topic er det MQTT klienten kigger efter, og reagerer på (se callback() længere nede)
const char *TOPIC = "esp/btn1";
// to_light bruges til at afgøre om LED'en skal lyse
// Da signalet fra disken sådan set bare er om der er strykket på knappen,
// er der ikke brug for andet end en bool
bool to_light = false;

// To objekter til forbindelse til MQTT serveren
// Client er navnet på den klient der står for netværksforbindelsen til MQTT serveren
// WifiClient er typen (da netværk tilgås via wifi)
WiFiClient client;
// mqttClient er den klient der står for den MQTT specifikke kommunikation
// PubSubClient er typen (da den kan publisere og subscribe)
PubSubClient mqttClient(client);

// Initialiser MQTT klient
void initMQTT(){
  // Sætter hvilken server og port der skal forespørges ved
  mqttClient.setServer(MQTT_HOST, MQTT_PORT);
  // Sætter hvilken funktion der skal køres som callback (se linje 105)
  mqttClient.setCallback(callback);

    // Så længe klienten ikke (! = not) er forbundet; kør loop
    // client.connected() vil returnere "false" inditl mqttClient er "logget ind"
    while (!client.connected()) {

      // Kør mqttClient.connect(), hvis den returnere "true":
      if (mqttClient.connect(MQTT_CLIENT_ID, MQTT_USER, MQTT_PASSWORD )) {
          // Print til serial monitor
          Serial.println("Connected to MQTT broker");
          // Blink LED hurtigt
          connection_blink(4);
      // Ellers, hvis den returnere "false":
      } else {
          // blink langsomt
          digitalWrite(D0, LOW); 
          delay(500);
          Serial.print(".");
          digitalWrite(D0, HIGH);
          delay(500);
      }
  }

  // Sæt klienten til at subscribe til TOPIC
  mqttClient.subscribe(TOPIC);

}

// Callback funktion, det er den funktion der bliver kørt,
// når der bliver registreret en ændring af TOPIC på MQTT serveren
void callback(char* topic, byte* payload, unsigned int length)
{
    // Variabel (array på længde "length") der skal holde den data der kommer fra MQTT serveren
    payload[length] = '\0';
    // Da der kun bruges en bool, laves "payload" til en string med String((char*) payload)
    // og derefter til en int med .toInt(), en int kan gemmes "direkte" som en bool
    // Ved at gøre det på denne måde, bliver alle andre tal-værdier end 0 til 1 (og 0 = 0)
    bool value = String((char*) payload).toInt();
    // Virker som overkill, men den simpleste metode jeg fandt.

    // Værdien kan her printes til serial monitor til debug
    // Serial.println(value);

    // Hvis "value" fra linje 72 er 1 (der ER trykket på knappen på disken)
    if (value){
      // Tænd LED
      to_light = true;
      // Mere debug
      // Serial.print("yes");
      
    // Ellers (hvis der IKKE er trykket)
    } else {
      // Sluk LED
      to_light = false;
      // Mere debug
      // Serial.print("no");
    }

    // Print HIGH i seral monitor hvis to_light er true, LOW hvis false
    // Serial.print(to_light ? "HIGH" : "LOW");
}

// Blink LED hurtigt. "times" er antalt gange der skal blinkes
void connection_blink(int times){
  // Mere debug
  // Serial.println("connection_blink running");
  // For loop
  // i starter som 0, hvis i er mindre end "times": fortsæt, forøg i med 1
  for (int i = 0; i < times; i++) {
    digitalWrite(LED_BUILTIN, HIGH);
    digitalWrite(D0, LOW); 
    delay(80);
    digitalWrite(LED_BUILTIN, LOW);
    digitalWrite(D0, HIGH);
    delay(80);
  }
}

// Setup funktionen er en default arduino funktion, der kører 1 gang ved startup.
// Den bruges til at initialisere forskellige funktioner (f.eks. seriel forbindelse og wifi)
// og sætte pin modes
void setup()
{
  // Sæt indbygget LED-pin til output funktion
  pinMode(LED_BUILTIN, OUTPUT);
  // Sæt D0 til output funktion
  pinMode(D0, OUTPUT);
  // Start serialforbindelse (som standart er det den over usb der bliver brugt)
  Serial.begin(115200);

  // Kør initWiFi (se linje 14)
  initWiFi();
  // Sluk for indbygget LED (ved ikke hvorfor jeg har gjort det.. men det skader ikke noget)
  digitalWrite(LED_BUILTIN, LOW);
  // Kør initMQTT (se linje 68)
  initMQTT();
  // Sluk for indbygget LED igen (ved heller ikke hvorfor jeg har gjort det.. men ja)
  digitalWrite(LED_BUILTIN, LOW);

}

// loop er også en default arduino funktion. Det er "main loopet" i programmet.
// Det starter efter "setup()" og starter forfra indtil enheden slukkes.
void loop() {
  // kør mqttClient.loop() 1 gang pr main loop
  // Det er den funktion der tjekker for ny data på de emner/nøgleord der er subscribet på,
  // og sender data'en hvis der er kørt en publish funktion
  mqttClient.loop();

  // Tænd eller sluk indbyget LED og LED forbundet til D0, afhængigt af hvad to_light er
  digitalWrite(LED_BUILTIN, to_light ? LOW : HIGH);
  digitalWrite(D0, to_light ? HIGH : LOW);
  // "bool-var ? output1 : output2" er en kort måde at skrive:
  // hvis bool-var er "true" returner output1, hvis "false" retuner output2

  // Den her kode/funktion tjekker efter om der stadig er forbindelse til wifi
  // Den er god til hvis raspberry pi'en er blevet slukket og tændt igen, uden at "arduinoerne" er blevet genstartet.
  // Sæt currentMillis til millis.
  // millis() returnere det antal millisekunder "arduinoen" har været tændt indtil nu
  unsigned long currentMillis = millis();
  // Tjek om status på wifi er "connected" OG om tidligere gemte "millis" minus netop gemte "millis",
  // er mere end fastsatte interval for tjek af wifi forbindelse
  if ((WiFi.status() != WL_CONNECTED) && (currentMillis - previousMillis >=interval)) {
    // Hvis det er; print millis() til serial monitor
    Serial.print(millis());
    // print besked om reconnect til serial monitor
    Serial.println("Wifi lost. Reconnecting...");
    // Kør disconnect af wifi (sætter nogle variabler i det underlæggende system)
    WiFi.disconnect();
    // Kør initWIFI igen (se linje 14)
    initWiFi();
    // Gem netop gemte "millis" til næste gang der tjekkes for wifi
    previousMillis = currentMillis;
    // Kør initMQTT igen (da forbindelse er tabt) (se linje 68)
    initMQTT();
  }

}
