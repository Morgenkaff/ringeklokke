# Ringeklokke

Use GPIO on Raspberry Pi, to control LED on ESP8266 via a MQTT broker running on same Pi.
Arduino project for the esp is under **esp8266_wifi_MQTT**, Python script interfacing between GPIOs and MQTT is **ringeklokke/ringeklokke.py** and different configuration files under **ringeklokke/confs**.
