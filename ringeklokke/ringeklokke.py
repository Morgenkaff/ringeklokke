#!/usr/bin/env python

import RPi.GPIO as GPIO
import time
import paho.mqtt.client as paho

# VARS START

# Debug vars
to_log = True

# MQTT
broker = 'localhost'
port = 1883
topic = "esp"
client_id = "pi"
username = 'esp'
password = 'esp-kode'

# GPIOs
led1 = 27
led2 = 17
led3 = 22
btn1 = 23
btn2 = 24
btn3 = 25
GPIO.setmode(GPIO.BCM)
GPIO.setup(led1, GPIO.OUT) # LED 1
GPIO.setup(led2, GPIO.OUT) # LED 2
GPIO.setup(led3, GPIO.OUT) # LED 3
GPIO.setup(btn1, GPIO.IN, GPIO.PUD_UP) # Button 1
GPIO.setup(btn2, GPIO.IN, GPIO.PUD_UP) # Button 2
GPIO.setup(btn3, GPIO.IN, GPIO.PUD_UP) # Button 3

## LED states
led1_state = False
led2_state = False
led3_state = False

# Timers
short_time = 0.1
medium_time = 0.4
long_time = 3

# VARS END

# Startup lights START
GPIO.output(led1, GPIO.LOW)
GPIO.output(led2, GPIO.LOW)
GPIO.output(led3, GPIO.LOW)
time.sleep(medium_time)
GPIO.output(led1, GPIO.HIGH)
time.sleep(short_time)
GPIO.output(led2, GPIO.HIGH)
time.sleep(short_time)
GPIO.output(led1, GPIO.LOW)
time.sleep(short_time)
GPIO.output(led3, GPIO.HIGH)
time.sleep(short_time)
GPIO.output(led2, GPIO.LOW)
time.sleep(short_time)
GPIO.output(led2, GPIO.HIGH)
time.sleep(short_time)
GPIO.output(led3, GPIO.LOW)
time.sleep(short_time)
GPIO.output(led1, GPIO.HIGH)
time.sleep(short_time)
GPIO.output(led2, GPIO.LOW)
time.sleep(short_time)
GPIO.output(led1, GPIO.LOW)

time.sleep(medium_time)

GPIO.output(led1, GPIO.HIGH)
GPIO.output(led2, GPIO.HIGH)
GPIO.output(led3, GPIO.HIGH)
time.sleep(short_time)
GPIO.output(led1, GPIO.LOW)
GPIO.output(led2, GPIO.LOW)
GPIO.output(led3, GPIO.LOW)
time.sleep(short_time)
GPIO.output(led1, GPIO.HIGH)
GPIO.output(led2, GPIO.HIGH)
GPIO.output(led3, GPIO.HIGH)
time.sleep(short_time)
GPIO.output(led1, GPIO.LOW)
GPIO.output(led2, GPIO.LOW)
GPIO.output(led3, GPIO.LOW)
time.sleep(short_time)

# Startup lights END

def log(s):
  if to_log:
    print(s)
    
def send_mqtt(t, s):
  client1 = paho.Client("control1")                   #create client object
  client1.on_publish = on_publish                     #assign function to callback
  client1.username_pw_set("esp", "esp-kode")
  client1.connect(broker,port)                        #establish connection
  ret = client1.publish(t,s)                 #publish
  
def on_publish(client,userdata,result):             #create function for callback
  print("data published \n")
  pass

# Logic loop
while True:

  if (not GPIO.input(btn1)): 
    
    led1_state = not led1_state
    
    GPIO.output(led1, led1_state)
    
    if (led1_state):
      send_mqtt("esp/btn1", 1)
    elif (not led1_state):
      send_mqtt("esp/btn1", 0)
      
    log("btn1 er: "+str(led1_state))
    
    time.sleep(medium_time)
    
  elif (not GPIO.input(btn2)):
    
    led2_state = not led2_state
    
    GPIO.output(led2, led2_state)
    
    if (led2_state):
      send_mqtt("esp/btn2", 1)
    elif (not led2_state):
      send_mqtt("esp/btn2", 0)
      
    log("btn1 er: "+str(led2_state))
    
    time.sleep(medium_time)
    
  elif (not GPIO.input(btn3)):
    
    led3_state = not led3_state
    
    GPIO.output(led3, led3_state)
    
    if (led3_state):
      send_mqtt("esp/btn3", 1)
    elif (not led3_state):
      send_mqtt("esp/btn3", 0)
      
    log("btn1 er: "+str(led3_state))
    
    time.sleep(medium_time)

  else:
    time.sleep(short_time)
    
  
  
